from unittest import TestCase
from selenium import webdriver

test_vars = {"main_url":'http://simplythetest-theinternet.herokuapp.com'}

class BaseTest(TestCase):

    def setUp(self):
        self._driver = webdriver.Firefox()

    def tearDown(self):
        self._driver.quit()