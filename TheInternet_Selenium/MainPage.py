from PageObject import PageObject
from selenium import webdriver

locators = {'heading':'.//h1'}

class MainPage(PageObject):

    def get_heading(self):
        return self._driver.find_element_by_xpath(locators['heading']).text