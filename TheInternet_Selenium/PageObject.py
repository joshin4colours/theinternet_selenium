from selenium import webdriver

test_vars = {'main_url':'http://simplythetest-theinternet.herokuapp.com'}

class PageObject():
    
    def __init__(self, driver):
        self._driver = driver

    def wait_until_loaded(self):
        pass

    def go_to(self, url):
        self._driver.get(url)

    def page_title(self):
        return self._driver.title