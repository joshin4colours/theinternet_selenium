from BaseTest import BaseTest
from PageObject import test_vars
from PageObject import PageObject
from MainPage import MainPage

class MainPageTest(BaseTest):

    def test_openPage(self):
        self.page = MainPage(self._driver)
        self.page.go_to(test_vars['main_url'])

        assert self.page.page_title() == 'The Internet'

    def test_checkHeadingText(self):
        self.page = MainPage(self._driver)
        self.page.go_to(test_vars['main_url'])

        assert self.page.get_heading() == 'Welcome to the Internet'